package com.example.lesson

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.lesson.databinding.CourseitemBinding
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream

class CoursesRecyclerViewAdapter(val list : List<CoursesDataItem>, val tags : List<TagsDataItem>, val checked : List<Boolean>) : RecyclerView.Adapter<CoursesRecyclerViewAdapter.VH>() {
    class VH(binding : CourseitemBinding) : RecyclerView.ViewHolder(binding.root) {
        val image = binding.imageView
        val course = binding.textView2
        val recycler = binding.recView
        val cost = binding.textView3
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(CourseitemBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.cost.text = "₽${list[position].price}"
        holder.course.text = list[position].title
        holder.recycler.adapter = TagsRecyclerAdapter(list[position].tags, tags, checked)

        val path = Environment.getExternalStorageDirectory()
        val file = File(path, list[position].cover)
        if (file.exists()) {
            holder.image.setImageBitmap(getFromFile(list[holder.adapterPosition].cover))
        } else {
            api.getImage(list[position].cover).enqueue(object : Callback<ResponseBody> {
                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    if (response.isSuccessful) {
                        setUpFile(response.body()!!, list[holder.adapterPosition].cover)
                        holder.image.setImageBitmap(getFromFile(list[holder.adapterPosition].cover))
                    } else {
                        Log.d(TAG, response.message())
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    Log.d(TAG, t.message!!)
                }
            })
        }
    }

    fun setUpFile(body: ResponseBody, name : String) {
        val path = Environment.getExternalStorageDirectory()
        val fileRender = ByteArray(4096)
        val inputStream = body.byteStream()
        val outputStream = FileOutputStream(File(path, name))
        while (true) {
            val sz = inputStream.read(fileRender)
            if (sz == -1) {
                break
            }
            outputStream.write(fileRender, 0, sz)
        }
        outputStream.flush()
        inputStream.close()
        outputStream.close()
    }

    fun getFromFile(name: String) : Bitmap {
        val path = Environment.getExternalStorageDirectory()
        val bitmap = BitmapFactory.decodeFile("$path/$name")
        return bitmap
    }
}