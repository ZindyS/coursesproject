package com.example.lesson

data class CoursesDataItem(
    val cover: String,
    val id: Int,
    val price: Int,
    val tags: List<Int>,
    val title: String
)