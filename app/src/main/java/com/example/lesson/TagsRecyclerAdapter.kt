package com.example.lesson

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.lesson.databinding.TagsitemBinding

class TagsRecyclerAdapter(val list : List<Int>, val tags : List<TagsDataItem>, val checked : List<Boolean>) : RecyclerView.Adapter<TagsRecyclerAdapter.VH>() {
    class VH(binding: TagsitemBinding) : RecyclerView.ViewHolder(binding.root) {
        val tag = binding.textView3
        val cardView = binding.background
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(TagsitemBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val cur = list[position]
        for (i in tags.indices) {
            if (tags[i].id == cur) {
                holder.tag.text = tags[i].name
                if (checked[i]) {
                    holder.cardView.setCardBackgroundColor(Color.parseColor("#FF9800"))
                }
                break
            }
        }

    }
}