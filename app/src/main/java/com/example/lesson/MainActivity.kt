package com.example.lesson

import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.Settings
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.viewbinding.BuildConfig
import com.example.lesson.databinding.ActivityMainBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream

//13.04.2023
//Солодков Роман
//Класс описания функционала экрана с курсами
class MainActivity : AppCompatActivity() {
    lateinit var courses : List<CoursesDataItem>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        Utils().setUpApi()
        binding.searchText.addTextChangedListener {
            val str = binding.searchText.text.toString().split(' ')
            for (i in tags.indices) {
                checked[i] = false
            }
            isAll = true
            for (i in str) {
                for (j in tags.indices) {
                    if (i.equals(tags[j].name, ignoreCase = true)) {
                        checked[j] = true
                        isAll = false
                    }
                }
            }
            if (isAll) {
                for (i in tags.indices) {
                    checked[i] = true
                }
            }
            update()
        }
        check()
    }

    fun check() {
        try {
            val path = Environment.getExternalStorageDirectory()
            val outputStream = FileOutputStream(File(path, "sd.jpg"))
            getTags()
        } catch (e: java.lang.Exception) {
            val intent = Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION,
                Uri.parse("package:" + com.example.lesson.BuildConfig.APPLICATION_ID))
            startActivityForResult(intent, 123)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 123) {
            check()
        }
    }

    fun getTags() {
        api.getTags().enqueue(object : Callback<TagsData> {
            override fun onResponse(call: Call<TagsData>, response: Response<TagsData>) {
                if (response.isSuccessful) {
                    tags = response.body()!!
                    for (i in tags.indices) {
                        checked.add(true)
                    }
                    getCourses()
                } else {
                    Toast.makeText(this@MainActivity, response.message(), Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<TagsData>, t: Throwable) {
                Toast.makeText(this@MainActivity, t.message!!, Toast.LENGTH_SHORT).show()
            }
        })
    }

    fun getCourses() {
        api.getCourses().enqueue(object : Callback<CoursesData> {
            override fun onResponse(call: Call<CoursesData>, response: Response<CoursesData>) {
                if (response.isSuccessful) {
                    courses = response.body()!!
                    binding.recyclerView.adapter = CoursesRecyclerViewAdapter(response.body()!!, tags, checked)
                    binding.recyclerView2.adapter = SearchTagsRecyclerView(tags, checked, isAll)
                } else {
                    Toast.makeText(this@MainActivity, response.message(), Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<CoursesData>, t: Throwable) {
                Toast.makeText(this@MainActivity, t.message!!, Toast.LENGTH_SHORT).show()
            }
        })
    }

    fun update() {
        val correctData = mutableListOf<CoursesDataItem>()
        for (i in courses) {
            for (j in tags.indices) {
                if (i.tags.contains(tags[j].id) && checked[j]) {
                    correctData.add(i)
                    break
                }
            }
        }

        binding.recyclerView2.adapter = SearchTagsRecyclerView(tags, checked, isAll)
        binding.recyclerView2.adapter!!.notifyDataSetChanged()
        binding.recyclerView.adapter = CoursesRecyclerViewAdapter(correctData, tags, checked)
        binding.recyclerView.adapter!!.notifyDataSetChanged()
    }

    companion object {
        lateinit var tags : List<TagsDataItem>
        lateinit var binding : ActivityMainBinding
        val checked = mutableListOf<Boolean>()
        var isAll = true
        fun onAllClicked() {
            binding.searchText.setText("")
        }

        fun onTagClicked(position : Int) {
            if (checked[position] && !isAll) {
                val str = binding.searchText.text.toString().split(' ')
                var newstr = ""
                for (i in str) {
                    if (i.toLowerCase() != tags[position].name.toLowerCase()) {
                        newstr += "$i "
                    }
                }
                binding.searchText.setText(newstr)
            } else {
                binding.searchText.setText(binding.searchText.text.toString() + " " + tags[position].name + " ")
            }
        }
    }
}