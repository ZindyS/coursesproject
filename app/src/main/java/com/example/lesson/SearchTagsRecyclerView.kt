package com.example.lesson

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings.LayoutAlgorithm
import androidx.recyclerview.widget.RecyclerView
import com.example.lesson.databinding.TagsitemBinding

class SearchTagsRecyclerView(val list : List<TagsDataItem>, val checked : List<Boolean>, val isAll : Boolean) : RecyclerView.Adapter<SearchTagsRecyclerView.VH>() {
    class VH(binding: TagsitemBinding) : RecyclerView.ViewHolder(binding.root) {
        val tag = binding.textView3
        val card = binding.background
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(TagsitemBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return list.size+1
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        if (position==0) {
            holder.tag.text = "Все"
            if (isAll) {
                holder.card.setCardBackgroundColor(Color.MAGENTA)
            }
            holder.card.setOnClickListener {
                MainActivity.onAllClicked()
            }
        } else {
            holder.tag.text = list[position-1].name
            if (checked[position-1] && !isAll) {
                holder.card.setCardBackgroundColor(Color.parseColor("#FF9800"))
            }
            holder.card.setOnClickListener {
                MainActivity.onTagClicked(position-1)
            }
        }
    }
}