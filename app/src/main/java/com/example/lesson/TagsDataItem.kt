package com.example.lesson

data class TagsDataItem(
    val id: Int,
    val name: String
)