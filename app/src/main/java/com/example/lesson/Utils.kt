package com.example.lesson

import android.content.Context
import android.content.SharedPreferences
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

lateinit var api : CodemaniAPI
val TAG = "errror"

class Utils {
    fun setUpApi() {
        val retrofit = Retrofit.Builder()
            .baseUrl("http://10.179.38.96:8085")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        api = retrofit.create(CodemaniAPI::class.java)
    }
}