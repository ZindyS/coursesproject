package com.example.lesson

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface CodemaniAPI {
    @GET("catalog/courses")
    fun getCourses() : Call<CoursesData>

    @GET("catalog/tags")
    fun getTags() : Call<TagsData>

    @POST("media")
    fun getImage(@Header("filename") filename : String) : Call<ResponseBody>
}